<?php

namespace Defesa\MitoseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('MitoseBundle:Default:index.html.twig', array('name' => $name));
    }
}
